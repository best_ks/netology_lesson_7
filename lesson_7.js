var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

//Задание 1. Создание массивов students и points.
var students=studentsAndPoints.filter(function (element){
    return typeof(element)=='string';
});

var points = studentsAndPoints.filter(function (element){
    return typeof(element)=='number';
});

//Задание 2. Вывод списка студентов без использования циклов.
console.log("Список студентов:");
students.forEach(function(element,i){
    return console.log("Студент %s набрал %d баллов", element, points[i]);
});

//Задание 3. Поиск студента, набравшего наибольшее количество баллов.
//max - текущее максимальное количество баллов
//maxIndex - фиксирует индекс элемента с максимальным количеством баллов
var max, maxIndex;
points.forEach(function(element,i){
    if(!max || max<element){
        max=element;
        maxIndex=i;
    }
});

console.log('\nСтудент, набравший максимальный балл: %s (%d баллов)', students[maxIndex], points[maxIndex]);

//Задание 3. Метод Reduce
//maxPoints - результат предыдущей итерации / первый элемент массива
//thisPoints - текущий элемент массива
//index - текущий индекс массива
//var max = 
    points.reduce(function(maxPoints, thisPoints, index){
    if(maxPoints<thisPoints){
        maxIndex = index;
        return thisPoints;
    }
    return maxPoints;
});

console.log('\nСтудент, набравший максимальный балл (метод Reduce): %s (%d баллов)', students[maxIndex], points[maxIndex]);

//Задание 4. Добавление 30 баллов студентам «Ирина Овчинникова» и «Александр Малов».
points = points.map(function(element, i){
    if(students[i]=='Ирина Овчинникова' || students[i]=='Александр Малов'){
        return element+30;
    }
    else return element;
});

console.log("\nДобавлены баллы. Список студентов:");
students.forEach(function(element,i){
    return console.log("Студент %s набрал %d баллов", element, points[i]);
});

//Дополнительное задание
var topMass = getTop(7);
topMass.forEach(function(element,i,arr){
    if(i%2==0)
			console.log('Студент '+arr[i]+' - '+arr[i+1]+' баллов');
});

//Функция getTop
function getTop(i){
	console.log("\nTop %d:", i);
    
	//копирование массива points[] с количеством баллов учащихся в переменную mass
    var mass = points.slice();
	
    //сортировка массива по убыванию
    mass.sort(function(a,b){
	   return -(a-b);
    });
    
    //изменение длины массива в соотвествии с запросом,
    //mass.length=i;
    mass = mass.slice(0,i);
    
    // Имеем числовой массив mass.
    // Буду добавлять имя студента перед соответствующим количеством баллов.
	mass.forEach(function(item, i, arr){
        // Сравниваю баллы текущего элемента mass и предыдущего 
        if(arr[i*2] == arr[i*2-1]){
            // Если баллы равны, то:
            // Смотрю имя последнего добавленного студента в массив mass
            var studentsName = arr[i*2-2];
            // Определяю индекс данного студента в массиве students по имени
            var index = students.indexOf(studentsName);
            // Определяю позицию элемента с данным ('arr[i*2]') количеством балов в массиве points, начиная с позиции index+1,
            // т.к. индекс index уже использован и имя студента из массива students с этим индексом уже добавлено в массив mass.
            // Нужно искать следующего студента с таким же количеством баллов.
            index = points.indexOf(arr[i*2], index+1);
            // Нахожу нужное имя студента в массиве students по новому индексу index
            studentsName = students[index];
            // Вставляю имя студента в массив mass на позицию i*2, 
            // т.е. перед соответствующим количесвом баллов.
            arr.splice(i*2, 0, studentsName);
        }
        else{
            // Если баллы не совпадают, то:
            // Ищу позицию в массиве points с данным ('arr[i*2]') колчеством баллов
            index = points.indexOf(arr[i*2]);
            // Определяю первое попавшееся имя студента в массиве students по данному индексу index
            studentsName = students[index];
            // Вставляю имя студента в массив mass на позицию i*2, 
            // т.е. перед соответствующим количесвом баллов.
            arr.splice(i*2, 0, studentsName);
        }
	});
	return mass;
}